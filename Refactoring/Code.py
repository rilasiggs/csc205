from random import randint #import randint for use of the randint function
from os import system  # Importing os to use system('clear') function
from time import sleep  # Importing time to use sleep function


def questions(amount): # Start one round of the game with {amount} number of questions
  score = 0
  print (f"\nOk, you have {amount} questions to answer \n")
  print ("Please type in the Capitals of these states.")
  print ("Please capitalize the first letter when typing out capitals.\n")
  states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakoda', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakoda', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming']
  capitals = ['Montgomery', 'Juneau', 'Phoenix', 'Little Rock', 'Sacramento', 'Denver', 'Hartford', 'Dover', 'Tallahassee', 'Atlanta', 'Honolulu', 'Boise', 'Springfeild', 'Indianapolis', 'Des Moines', 'Topeka', 'Frankfort', 'Balton Rouge', 'Augusta', 'Annapolis', 'Boston', 'Lansing', 'Saint Paul', 'Jackson', 'Jefferson City', 'Helena', 'Lincoln', 'Carson City', 'Concord', 'Trenton', 'Santa Fe', 'Albany', 'Raleigh', 'Bismark', 'Columbus', 'Oklahoma City', 'Salem', 'Harrisburg', 'Providence', 'Columbia', 'Pierre', 'Nashville', 'Austin', 'Salt Lake City', 'Montpelier', 'Richmond', 'Olympia', 'Charleston', 'Madison', 'Cheyenne']
  wrongques = ""
  
  while amount >= 1: # While there is one or more questions left
    randpair = randint(1, len(capitals) + 1)
    quesinput = input(states[randpair] + "\n")
    quesanswer = capitals[randpair]
    
    if quesinput == quesanswer: # If the user got the correct answer
      print("\nCorrect!")
      score += 1
    else: # If the user got hte incorrect answer
      print("\nIncorrect. \n")
      wrongques = wrongques + "\n" + states[randpair]

    states.remove(states[randpair])
    capitals.remove(capitals[randpair])
    amount = amount - 1
  print(f"\nYour score is {score}.")
  print("The states with capitals you do not know are: " + wrongques)

  return

def game(rounds):
  amount = int(input("How many questions do you want to be asked? (Remember there are only 50 states) "))
  if amount > 0 and amount <= 50:
    while rounds >=1:
      rounds -= 1
      questions(amount) # Call function questions with parameter amount
      if rounds != 0:
        print("Next round in 5 seconds")
        sleep(5)
        system('clear')
      else: 
        print("\nThanks for playing!")
      
  else:
    print("Enter a number between 1-50 next time.")





numba = int(input("How many rounds do you want to play? "))
game(numba)