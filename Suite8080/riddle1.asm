;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; NAME:    riddle1.asm
; AUTHOR:  Stolen directly from Kevin Cole by Jeff Elkner 
; LASTMOD: 2021.11.03 (je)
;
; DESCRIPTION:
;
;     Not sure what it does yet.  The purpose of the riddle is to figure that 
;     out.
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Code segment

    LDA    ML1    ; Load accumulator with byte at address ML1
    STA    ML2    ; Store byte in the accumlator to address ML2
    LDA    ML3    ; Load accumulator with byte at address ML3
    STA    ML4    ; Store byte in the accumlator to address ML4
    LDA    ML5    ; Load accumulator with byte at address ML5
    STA    ML6    ; Store byte in the accumlator to address ML6
    JMP    ML1    ; Unconditionally jump to address ML1

; Data segment

    ML1:  DW  000o    ; First memory location
    ML2:  DW  200o    ; Second memory location
    ML3:  DW  001o    ; Third memory location
    ML4:  DW  201o    ; Fourth memory location
    ML5:  DW  002o    ; Fifth memory location
    ML6:  DW  202o    ; Sixth memory location

    END               ; End
