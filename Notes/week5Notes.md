# Notes:

- Installed suite8080 using "pip3 install suite8080"
- suite8080 did not work and I could not use asm80

- realized this was because suite8080 was not in PATH

- where is it then?
- used  "find . -name "asm80" 2>/dev/null" in my home directory to locate where pip installed suite8080
- "." searches the current directory, -name asm80 looks for files named asm80, and 2>/dev/null 
