# Notes:
## Encoders:
- Encoders have n outputs and 2^n inputs
- 4 inputs = 2 outputs

![](images/encoder.jpg)

Types of encoders:
1. Priority
2. Decimal

## Decoders:
- Decoders perform the reverse operation of encoders
- Decoders take coded inputs and decode them to there original values, undoing the process of an encoder
