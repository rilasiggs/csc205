# Notes:
We initially used the online Logic Gate Simulator to understand half adder and full adder circuits, then we used simulIDE to remake the Full Adder and build an 8 bit adder.
## Logic Gate Simulator
Half Adder Circuit:
![](images/half_adder.png)

Full Adder Circuit:
![](images/full_adder.png)

## SimulIDE
Full Adder:
![](images/simulIDE_FullAdder.png)

8 Bit Adder:
![](images/simulIDE_8BitAdder.png)
