# My Notes:
## First Notes
- George Boole wrote about the connections between math and logic in his booklet "The Mathmatical Analysis of Logic".
- Boolean algebra assumes a statement is either true or false.
- An "And" switch only returns true if both of its inputs are true.
- An "Or" switch returns true if any of its inputs are true.
- A "Not" switch toggles its input from true to false and vice versa.
- "Xor" aka exclusive or switch returns true when exactly one of its inputs is true. It is also known as the binary adder.
- The Altair 8800 performs operations in binary.
- In binary, every bit can be a 1 or 0, and it represents a certain power of two.
- In octal, binary numbers are split into groups of three bits, and the decimal of each group is one digit in octal.
- The software for the altair must be loaded in 8-bit words known as machine language.
-. In computer programming, the steps are \`Define the problem, establish an  approach, and write the program\`.
- A flow diagram can illustrate these steps.
- The altair can store special programs in its memory and access them later from the main program; these special programs are known as subroutines.

## add2numbers.bin code:
1. loads a number from memory into the accumulator
2. moves that number to register B
3. load the accumulator with a different number from memory
4. add the contents of the accumulator and register B, replace the accumulator with the sum
5. store accumulator contents in memory
6. jump back to the start and repeat

## add3humbers.bin code:
1. perform steps 1-5 of add2numbers.bin
2. load a third number into the accumulator from memory
3. move the contents of the accumulator to register B
4. load the accumulator with the sum of the first two numbers from steps 1-5 of add2numbers.bin
5. add the register to the accumulator and store the sum in the accumulator
6. store the store accumulator contents in memory (this is the sum of all three numbers)
7. jump to the start and repeat

## compare2numbers.bin code:
1. load accumulator with memory address
2. move accumulator to register
3. load accumulator with another memory address
4. compare accumulator contents to register contents
